package com.seop.shoppingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingAPiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingAPiApplication.class, args);
	}

}
