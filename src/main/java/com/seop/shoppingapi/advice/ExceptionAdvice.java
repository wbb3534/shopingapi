package com.seop.shoppingapi.advice;

import com.seop.shoppingapi.enums.ResultCode;
import com.seop.shoppingapi.exception.CMissingDataException;
import com.seop.shoppingapi.exception.CMissingPasswordException;
import com.seop.shoppingapi.exception.CMissingUsernameException;
import com.seop.shoppingapi.exception.CWrongPhoneNumberException;
import com.seop.shoppingapi.model.CommonResult;
import com.seop.shoppingapi.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CMissingUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingUsernameException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_USERNAME);
    }

    @ExceptionHandler(CMissingPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingPasswordException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_PASSWORD);
    }

}

