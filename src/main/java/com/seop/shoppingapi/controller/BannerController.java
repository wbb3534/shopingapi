package com.seop.shoppingapi.controller;

import com.seop.shoppingapi.model.*;
import com.seop.shoppingapi.service.BannerService;
import com.seop.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "배너 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/banner")
public class BannerController {
    private final BannerService bannerService;

    @ApiOperation(value = "배너 등록")
    @PostMapping("/new")
    public CommonResult setBanner(@RequestBody @Valid BannerRequest request) {
        bannerService.setBanner(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "배너 리스트")
    @GetMapping("/list")
    public ListResult<BannerItem> getBanners() {
        return ResponseService.getListResult(bannerService.getBanners(), true);
    }

    @ApiOperation(value = "배너 업데이트")
    @PutMapping("/update/{id}")
    public CommonResult putBanner(@PathVariable("id")long id, @RequestBody @Valid BannerUpdateRequest request) {
        bannerService.putBanner(id, request);

        return ResponseService.getSuccessResult();
    }
}
