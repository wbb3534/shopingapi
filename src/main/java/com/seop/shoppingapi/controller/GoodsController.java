package com.seop.shoppingapi.controller;

import com.seop.shoppingapi.enums.Category;
import com.seop.shoppingapi.model.*;
import com.seop.shoppingapi.service.GoodsService;
import com.seop.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "상품 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/goods")
public class GoodsController {
    private final GoodsService goodsService;

    @ApiOperation(value = "상품 등록")
    @PostMapping("/new")
    public CommonResult setGoods(@RequestBody @Valid GoodsRequest request) {
        goodsService.setGoods(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "상품 리스트")
    @GetMapping("/list")
    public ListResult<GoodsItem> getGoods(@RequestParam("category")Category category) {
        return ResponseService.getListResult(goodsService.getGoods(category), true);
    }

    @ApiOperation(value = "상품 수정")
    @PutMapping("/update/{id}")
    public CommonResult putGoods(@PathVariable("id") long id, @RequestBody @Valid GoodsUpdateRequest request) {
        goodsService.putGoods(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "상품 삭제")
    @DeleteMapping("/del/{id}")
    public CommonResult delGoods(@PathVariable("id") long id) {
        goodsService.delGoods(id);

        return ResponseService.getSuccessResult();
    }
}
