package com.seop.shoppingapi.controller;

import com.seop.shoppingapi.model.LoginRequest;
import com.seop.shoppingapi.model.LoginResponse;
import com.seop.shoppingapi.model.SingleResult;
import com.seop.shoppingapi.service.MemberService;
import com.seop.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "멤버 관리")
@RequestMapping("/v1/member")
@RestController
@RequiredArgsConstructor
public class MemberController {
    private final MemberService memberService;

    @ApiModelProperty(value = "로그인")
    @PostMapping("/login")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest request) {
        return ResponseService.getSingleResult(memberService.doLogin(request));
    }
}
