package com.seop.shoppingapi.controller;

import com.seop.shoppingapi.entity.Goods;
import com.seop.shoppingapi.model.CommonResult;
import com.seop.shoppingapi.model.ListResult;
import com.seop.shoppingapi.model.MyCartItem;
import com.seop.shoppingapi.model.MyCartRequest;
import com.seop.shoppingapi.service.GoodsService;
import com.seop.shoppingapi.service.MyCartService;
import com.seop.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "장바구니 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/my-cart")
public class MyCartController {
    private final MyCartService myCartService;
    private final GoodsService goodsService;

    @ApiOperation(value = "장바구니 등록")
    @PostMapping("/new")
    public CommonResult setMyCart(@RequestBody @Valid MyCartRequest request) {
        Goods goodsId = goodsService.getId(request.getMemberId());

        myCartService.setMyCart(request.getMemberId(), goodsId);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "장바구니 멤버아이디별 리스트")
    @GetMapping("/list/member-id/{memberId}")
    public ListResult<MyCartItem> getMyCarts(@PathVariable("memberId") long memberId) {
        return ResponseService.getListResult(myCartService.getMyCarts(memberId), true);
    }

    @ApiOperation(value = "장바구니 상품 삭제")
    @DeleteMapping("goods//cart-id/{id}")
    public CommonResult delMyCart(@PathVariable("id") long id) {
        myCartService.delMyCart(id);

        return ResponseService.getSuccessResult();
    }
}
