package com.seop.shoppingapi.entity;

import com.seop.shoppingapi.interfaces.CommonModelBuilder;
import com.seop.shoppingapi.model.BannerRequest;
import com.seop.shoppingapi.model.BannerUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Banner {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "배너 이미지주소")
    @Column(nullable = false, length = 200)
    private String bannerImageUrl;
    @ApiModelProperty(notes = "배너 사용여부")
    @Column(nullable = false)
    private Boolean isUse;

    public void putIsUseBanner(BannerUpdateRequest request) {
        this.bannerImageUrl = request.getBannerImageUrl();
        this.isUse = request.getIsUse();
    }

    private Banner(BannerBuilder builder) {
        this.bannerImageUrl = builder.bannerImageUrl;
        this.isUse = builder.isUse;
    }
    public static class BannerBuilder implements CommonModelBuilder<Banner> {
        private final String bannerImageUrl;
        private final Boolean isUse;

        public BannerBuilder(BannerRequest request) {
            this.bannerImageUrl = request.getBannerImageUrl();
            this.isUse = false;
        }
        @Override
        public Banner build() {
            return new Banner(this);
        }
    }
}
