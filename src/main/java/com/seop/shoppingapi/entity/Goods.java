package com.seop.shoppingapi.entity;

import com.seop.shoppingapi.enums.Category;
import com.seop.shoppingapi.interfaces.CommonModelBuilder;
import com.seop.shoppingapi.model.GoodsRequest;
import com.seop.shoppingapi.model.GoodsUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Goods {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "카테고리 enums")
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 30)
    private Category category;
    @ApiModelProperty(notes = "상품 이미지 주소")
    @Column(nullable = false, length = 200)
    private String goodsImageUrl;
    @ApiModelProperty(notes = "상품 이름")
    @Column(nullable = false, length = 50)
    private String goodsName;
    @ApiModelProperty(notes = "상품 가격")
    @Column(nullable = false)
    private Double goodsPrice;

    @ApiModelProperty(notes = "할인 가격")
    @Column(nullable = false)
    private Double salePrice;

    public void putGoods(GoodsUpdateRequest request) {
        this.category = request.getCategory();
        this.goodsImageUrl = request.getGoodsImageUrl();
        this.goodsName = request.getGoodsName();
        this.goodsPrice = request.getGoodsPrice();
        this.salePrice = request.getSalePrice();
    }

    private Goods(GoodsBuilder builder) {
        this.category = builder.category;
        this.goodsImageUrl = builder.goodsImageUrl;
        this.goodsName = builder.goodsName;
        this.goodsPrice = builder.goodsPrice;
        this.salePrice = builder.salePrice;
    }

    public static class GoodsBuilder implements CommonModelBuilder<Goods> {
        private final Category category;
        private final String goodsImageUrl;
        private final String goodsName;
        private final Double goodsPrice;
        private final Double salePrice;

        public GoodsBuilder(GoodsRequest request) {
            this.category = request.getCategory();
            this.goodsImageUrl = request.getGoodsImageUrl();
            this.goodsName = request.getGoodsName();
            this.goodsPrice = request.getGoodsPrice();
            this.salePrice = request.getSalePrice();
        }
        @Override
        public Goods build() {
            return new Goods(this);
        }
    }
}
