package com.seop.shoppingapi.entity;

import com.seop.shoppingapi.interfaces.CommonModelBuilder;
import com.seop.shoppingapi.model.LoginRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "고객명")
    @Column(nullable = false, length = 20)
    private String name;

    @ApiModelProperty(value = "아이디")
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    @ApiModelProperty(value = "비밀번호")
    @Column(nullable = false, length = 50)
    private String password;

    private Member(MemberBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String username;
        private final String password;

        public MemberBuilder(LoginRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
        }
        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
