package com.seop.shoppingapi.entity;

import com.seop.shoppingapi.interfaces.CommonModelBuilder;
import com.seop.shoppingapi.model.MyCartRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyCart {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "회원 시퀀스")
    @Column(nullable = false)
    private Long memberId;

    @ApiModelProperty(value = "상품 정보 entity join")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "goodsId",nullable = false)
    private Goods goods;

    private MyCart(MyCartBuilder builder) {
        this.memberId = builder.memberId;
        this.goods = builder.goods;
    }

    public static class MyCartBuilder implements CommonModelBuilder<MyCart> {
        private final Long memberId;
        private final Goods goods;

        public MyCartBuilder(long memberId, Goods goods) {
            this.memberId = memberId;
            this.goods = goods;
        }

        @Override
        public MyCart build() {
            return new MyCart(this);
        }
    }
}
