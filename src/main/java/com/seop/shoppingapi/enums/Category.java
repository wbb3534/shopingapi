package com.seop.shoppingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    LIFE("생활")
    , CLOTHING("의류")
    , BEAUTY("뷰티")
    , FOOD("식품")
    ;
    private final String name;
}
