package com.seop.shoppingapi.exception;

public class CMissingPasswordException extends RuntimeException {
    public CMissingPasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMissingPasswordException(String msg) {
        super(msg);
    }

    public CMissingPasswordException() {
        super();
    }
}
