package com.seop.shoppingapi.exception;

public class CMissingUsernameException extends RuntimeException {
    public CMissingUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMissingUsernameException(String msg) {
        super(msg);
    }

    public CMissingUsernameException() {
        super();
    }
}
