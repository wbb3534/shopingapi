package com.seop.shoppingapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
