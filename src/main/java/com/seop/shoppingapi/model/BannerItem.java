package com.seop.shoppingapi.model;

import com.seop.shoppingapi.entity.Banner;
import com.seop.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BannerItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;
    @ApiModelProperty(notes = "배너 이미지주소")
    private String bannerImageUrl;
    @ApiModelProperty(notes = "배너 사용여부")
    private Boolean isUse;

    private BannerItem(BannerItemBuilder builder) {
        this.id = builder.id;
        this.bannerImageUrl = builder.bannerImageUrl;
        this.isUse = builder.isUse;
    }
    public static class BannerItemBuilder implements CommonModelBuilder<BannerItem> {
        private final Long id;
        private final String bannerImageUrl;
        private final Boolean isUse;

        public BannerItemBuilder(Banner banner) {
            this.id = banner.getId();
            this.bannerImageUrl = banner.getBannerImageUrl();
            this.isUse = banner.getIsUse();
        }
        @Override
        public BannerItem build() {
            return new BannerItem(this);
        }
    }
}
