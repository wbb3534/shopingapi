package com.seop.shoppingapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BannerUpdateRequest {
    @ApiModelProperty(notes = "배너 이미지주소(~200글자)")
    @NotNull
    @Length(min = 1, max = 200)
    private String bannerImageUrl;

    @ApiModelProperty(notes = "배너 사용여부")
    @NotNull
    private Boolean isUse;
}
