package com.seop.shoppingapi.model;

import com.seop.shoppingapi.entity.Goods;
import com.seop.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GoodsItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;
    @ApiModelProperty(notes = "카테고리 이름")
    private String category;
    @ApiModelProperty(notes = "상품 이미지 주소")
    private String goodsImageUrl;
    @ApiModelProperty(notes = "상품 이름")
    private String goodsName;
    @ApiModelProperty(notes = "상품 가격")
    private Double goodsPrice;

    @ApiModelProperty(notes = "할인 가격")
    @Column(nullable = false)
    private Double salePrice;
    @ApiModelProperty(notes = "상품할인된가격")
    private Double saleResultPrice;

    private GoodsItem(GoodsItemBuilder builder) {
        this.id = builder.id;
        this.category = builder.category;
        this.goodsImageUrl = builder.goodsImageUrl;
        this.goodsName = builder.goodsName;
        this.goodsPrice = builder.goodsPrice;
        this.salePrice = builder.salePrice;
        this.saleResultPrice = builder.saleResultPrice;
    }
    public static class GoodsItemBuilder implements CommonModelBuilder<GoodsItem> {
        private final Long id;
        private final String category;
        private final String goodsImageUrl;
        private final String goodsName;
        private final Double goodsPrice;
        private final Double salePrice;
        private final Double saleResultPrice;

        public GoodsItemBuilder(Goods goods) {
            this.id = goods.getId();
            this.category = goods.getCategory().getName();
            this.goodsImageUrl = goods.getGoodsImageUrl();
            this.goodsName = goods.getGoodsName();
            this.goodsPrice = goods.getGoodsPrice();
            this.salePrice = goods.getSalePrice();
            this.saleResultPrice = goods.getGoodsPrice() - goods.getSalePrice();
        }

        @Override
        public GoodsItem build() {
            return new GoodsItem(this);
        }
    }
}
