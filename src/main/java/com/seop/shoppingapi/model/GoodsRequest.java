package com.seop.shoppingapi.model;

import com.seop.shoppingapi.enums.Category;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GoodsRequest {
    @ApiModelProperty(notes = "카테고리 enums")
    @Enumerated(EnumType.STRING)
    @NotNull
    private Category category;
    @ApiModelProperty(notes = "상품 이미지 주소")
    @NotNull
    @Length(min = 1, max = 200)
    private String goodsImageUrl;
    @ApiModelProperty(notes = "상품 이름")
    @NotNull
    @Length(min = 1, max = 50)
    private String goodsName;
    @ApiModelProperty(notes = "상품 가격")
    @NotNull
    private Double goodsPrice;

    @ApiModelProperty(notes = "할인 가격")
    @Column(nullable = false)
    private Double salePrice;
}
