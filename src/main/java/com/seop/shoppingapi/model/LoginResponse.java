package com.seop.shoppingapi.model;

import com.seop.shoppingapi.entity.Member;
import com.seop.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    @ApiModelProperty(value = "시퀀스")
    private Long id;

    @ApiModelProperty(value = "고객명")
    private String name;

    private LoginResponse(LoginResponseBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }
    public static class LoginResponseBuilder implements CommonModelBuilder<LoginResponse> {
        private final Long id;
        private final String name;

        public LoginResponseBuilder(Member member) {
            this.id = member.getId();
            this.name = member.getName();
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
