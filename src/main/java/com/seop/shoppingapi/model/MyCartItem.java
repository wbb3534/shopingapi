package com.seop.shoppingapi.model;

import com.seop.shoppingapi.entity.MyCart;
import com.seop.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyCartItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;

    @ApiModelProperty(value = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(value = "상품 시퀀스")
    private Long goodsId;

    @ApiModelProperty(notes = "카테고리 이름")
    private String category;

    @ApiModelProperty(notes = "상품 이미지 주소")
    private String goodsImageUrl;

    @ApiModelProperty(notes = "상품 이름")
    private String goodsName;

    @ApiModelProperty(notes = "상품 가격")
    private Double goodsPrice;

    @ApiModelProperty(notes = "할인 가격")
    @Column(nullable = false)
    private Double salePrice;

    @ApiModelProperty(notes = "상품할인된가격")
    private Double saleResultPrice;

    private MyCartItem(MyCartItemBuilder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.goodsId = builder.goodsId;
        this.category = builder.category;
        this.goodsImageUrl = builder.goodsImageUrl;
        this.goodsName = builder.goodsName;
        this.goodsPrice = builder.goodsPrice;
        this.salePrice = builder.salePrice;
        this.saleResultPrice = builder.saleResultPrice;
    }
    public static class MyCartItemBuilder implements CommonModelBuilder<MyCartItem> {
        private final Long id;
        private final Long memberId;
        private final Long goodsId;
        private final String category;
        private final String goodsImageUrl;
        private final String goodsName;
        private final Double goodsPrice;
        private final Double salePrice;
        private final Double saleResultPrice;

        public MyCartItemBuilder(MyCart myCart) {
            this.id = myCart.getId();
            this.memberId = myCart.getMemberId();
            this.goodsId = myCart.getGoods().getId();
            this.category = myCart.getGoods().getCategory().getName();
            this.goodsImageUrl = myCart.getGoods().getGoodsImageUrl();
            this.goodsName = myCart.getGoods().getGoodsName();
            this.goodsPrice = myCart.getGoods().getGoodsPrice();
            this.salePrice = myCart.getGoods().getSalePrice();
            this.saleResultPrice = myCart.getGoods().getSalePrice();
        }
        @Override
        public MyCartItem build() {
            return new MyCartItem(this);
        }
    }
}
