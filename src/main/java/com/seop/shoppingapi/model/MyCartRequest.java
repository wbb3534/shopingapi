package com.seop.shoppingapi.model;

import com.seop.shoppingapi.entity.Goods;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MyCartRequest {
    @ApiModelProperty(value = "회원 시퀀스")
    @NotNull
    private Long memberId;

    @ApiModelProperty(value = "상품")
    @NotNull
    private Long goodsId;

}
