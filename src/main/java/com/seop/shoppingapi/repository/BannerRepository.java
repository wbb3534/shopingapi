package com.seop.shoppingapi.repository;

import com.seop.shoppingapi.entity.Banner;
import com.seop.shoppingapi.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BannerRepository extends JpaRepository<Banner, Long> {
    List<Banner> findAllByIdGreaterThanEqualOrderByIdAsc(long id);
}
