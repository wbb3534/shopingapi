package com.seop.shoppingapi.repository;

import com.seop.shoppingapi.entity.Goods;
import com.seop.shoppingapi.enums.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoodsRepository extends JpaRepository<Goods, Long> {
    List<Goods> findAllByIdGreaterThanEqualOrderByIdAsc(long id);
    List<Goods> findAllByCategoryOrderByIdAsc(Category category);
}
