package com.seop.shoppingapi.repository;

import com.seop.shoppingapi.entity.MyCart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MyCartRepository extends JpaRepository<MyCart, Long> {
    List<MyCart> findAllByMemberIdOrderByIdDesc(Long memberId);
}
