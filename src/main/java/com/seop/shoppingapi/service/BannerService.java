package com.seop.shoppingapi.service;

import com.seop.shoppingapi.entity.Banner;
import com.seop.shoppingapi.exception.CMissingDataException;
import com.seop.shoppingapi.model.BannerItem;
import com.seop.shoppingapi.model.BannerRequest;
import com.seop.shoppingapi.model.BannerUpdateRequest;
import com.seop.shoppingapi.model.ListResult;
import com.seop.shoppingapi.repository.BannerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BannerService {
    private final BannerRepository bannerRepository;

    public void setBanner(BannerRequest request) {
        Banner addItem = new Banner.BannerBuilder(request).build();

        bannerRepository.save(addItem);
    }

    public ListResult<BannerItem> getBanners() {
        List<Banner> originList = bannerRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L);

        List<BannerItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new BannerItem.BannerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putBanner(long id, BannerUpdateRequest request) {
        Banner originData = bannerRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putIsUseBanner(request);

        bannerRepository.save(originData);
    }
}
