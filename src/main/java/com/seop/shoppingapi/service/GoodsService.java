package com.seop.shoppingapi.service;

import com.seop.shoppingapi.entity.Goods;
import com.seop.shoppingapi.enums.Category;
import com.seop.shoppingapi.exception.CMissingDataException;
import com.seop.shoppingapi.model.GoodsItem;
import com.seop.shoppingapi.model.GoodsRequest;
import com.seop.shoppingapi.model.GoodsUpdateRequest;
import com.seop.shoppingapi.model.ListResult;
import com.seop.shoppingapi.repository.GoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodsService {
    public final GoodsRepository goodsRepository;

    public void setGoods(GoodsRequest request) {
        Goods addData = new Goods.GoodsBuilder(request).build();

        goodsRepository.save(addData);
    }

    public ListResult<GoodsItem> getGoods(Category category) {
        List<Goods> originList = goodsRepository.findAllByCategoryOrderByIdAsc(category);

        List<GoodsItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new GoodsItem.GoodsItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putGoods(long id, GoodsUpdateRequest request) {
        Goods origin = goodsRepository.findById(id).orElseThrow(CMissingDataException::new);

        origin.putGoods(request);

        goodsRepository.save(origin);
    }

    public void delGoods(long id) {
        goodsRepository.deleteById(id);
    }

    public Goods getId(long id) {
        return goodsRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}
