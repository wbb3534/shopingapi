package com.seop.shoppingapi.service;

import com.seop.shoppingapi.entity.Member;
import com.seop.shoppingapi.exception.CMissingDataException;
import com.seop.shoppingapi.exception.CMissingPasswordException;
import com.seop.shoppingapi.exception.CMissingUsernameException;
import com.seop.shoppingapi.model.LoginRequest;
import com.seop.shoppingapi.model.LoginResponse;
import com.seop.shoppingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public LoginResponse doLogin(LoginRequest request) {
        Member member = memberRepository.findByUsername(request.getUsername()).orElseThrow(CMissingUsernameException::new);

        if (!member.getPassword().equals(request.getPassword())) throw new CMissingPasswordException();

        return new LoginResponse.LoginResponseBuilder(member).build();
    }
}
