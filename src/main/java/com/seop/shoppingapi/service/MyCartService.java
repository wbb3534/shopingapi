package com.seop.shoppingapi.service;

import com.seop.shoppingapi.entity.Goods;
import com.seop.shoppingapi.entity.MyCart;
import com.seop.shoppingapi.model.ListResult;
import com.seop.shoppingapi.model.MyCartItem;
import com.seop.shoppingapi.model.MyCartRequest;
import com.seop.shoppingapi.repository.MyCartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MyCartService {
    private final MyCartRepository myCartRepository;

    public void setMyCart(long memberId, Goods goods) {
        MyCart addData = new MyCart.MyCartBuilder(memberId, goods).build();

        myCartRepository.save(addData);
    }

    public ListResult<MyCartItem> getMyCarts(long memberId) {
        List<MyCart> originList = myCartRepository.findAllByMemberIdOrderByIdDesc(memberId);

        List<MyCartItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new MyCartItem.MyCartItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void delMyCart(long id) {
        myCartRepository.deleteById(id);
    }
}
